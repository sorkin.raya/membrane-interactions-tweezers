import os
from datetime import timedelta
from nptdms import TdmsFile
import pandas as pd
import numpy as np
from matplotlib import pyplot as pp
from logging import warning
from collections import defaultdict
import re
from scipy.stats import linregress

PATH = r'E:\Raya\20171129\Force'
SPIKE_THRESHOLD = -25  # pN
STEP_THRESHOLD = 5  # pN
TIME_OF_CYCLE = 19.5 #13.6  # seconds
TIME_OF_TETHER = 3  # seconds

resutls_path = os.path.join(PATH, 'results')

if not os.path.exists(resutls_path):
    os.mkdir(resutls_path)


def calc_repetition_time(data):
    force_freq = np.fft.rfft(data.F)
    force_freq[0] = 0
    max_force_freq_idx = np.argmax(force_freq)
    print('should have %d repetitions' % max_force_freq_idx)
    dt = data.index[-1].total_seconds() - data.index[0].total_seconds()
    return dt / max_force_freq_idx


def load_tdms_data(file_name):
    tdms_file = TdmsFile(file_name)
    data = pd.DataFrame({
        't': pd.to_timedelta(tdms_file.object('FD Data', 'Time (ms)').data, 'ms'),
        'F': tdms_file.object('FD Data', 'Force Channel 0 (pN)').data,
        'x1': tdms_file.object('FD Data', 'Bead 1 X position (um)').data,
        'x2': tdms_file.object('FD Data', 'Bead 2 X position (um)').data
    }).set_index('t')
    data = data.sort_index()
    return data[~data.index.duplicated(keep='first')]


def get_spikes_start_end(data):
    spike_start_end_points = (data.F < SPIKE_THRESHOLD).astype(int).diff()
    spike_start_times = spike_start_end_points.index[spike_start_end_points > 0]
    spike_end_times = spike_start_end_points.index[spike_start_end_points < 0]
    end_validity_mask = pd.Series([False]*spike_end_times.shape[0])
    start_validity_mask = pd.Series([True]*spike_start_times.shape[0])
    final_step = False
    for start_spike_idx, spike_start_time in enumerate(spike_start_times):
        if start_spike_idx > 0 and spike_start_time - spike_start_times[start_spike_idx-1] < timedelta(seconds=5):
            start_validity_mask[start_spike_idx] = False
            continue
        try:
            end_spike_idx = spike_end_times.get_loc(spike_start_time, method='bfill')
        except:
            if data[spike_start_time:].F.max() > SPIKE_THRESHOLD:
                end_spike_idx = spike_end_times.shape[0]
                spike_end_times = spike_end_times.append(pd.TimedeltaIndex([spike_start_time + timedelta(seconds=1)]))
                final_step = True
            else:
                start_validity_mask[start_spike_idx] = False
        spike_end_time = spike_end_times[end_spike_idx]
        spike_time_length = spike_end_time - spike_start_time
        if spike_time_length > timedelta(seconds=TIME_OF_TETHER) or spike_time_length < timedelta(seconds=0):
            print('could not find end for spike starting at %s' % spike_start_time)
            start_validity_mask[start_spike_idx] = False
        else:
            print('found spike at %s - %s' % (spike_start_time, spike_end_time))
            end_validity_mask[end_spike_idx] = True
        if final_step:
            break
    if end_validity_mask.sum() != start_validity_mask.sum():
        raise Exception(
            'spike start and end mismatch - found %d starts and %d ends - probably bad file' % (
                start_validity_mask.sum(), end_validity_mask.sum()
            ))
    return spike_start_times[start_validity_mask], spike_end_times[end_validity_mask]


def append_results(results, entry_name, spike_height, spike_start_time, step_height):
    results['entry_name'].append(entry_name)
    results['spike_max'].append(spike_height)
    results['spike_time'].append(spike_start_time)
    results['step_mean'].append(step_height)
    results['spike_delta'].append(spike_height - step_height)


def append_spike_reslts(results, entry_name, slope, intercept, r_value, p_value, std_err):
    results['entry_name'].append(entry_name)
    results['slope'].append(slope)
    results['intercept'].append(intercept)
    results['r_value'].append(r_value)
    results['p_value'].append(p_value)
    results['std_err'].append(std_err)


pp.ioff()
mean_values_per_group = defaultdict(list)
data_query = re.compile(r'(?P<date>\d{8})[a-z\s\d\-]*#(?P<start_idx>\d+)-(?P<end_idx>\d+)', re.IGNORECASE)
results = defaultdict(list)
spike_results = defaultdict(list)
for file_name in os.listdir(PATH):
    entry_name = None
    try:
        if file_name.endswith('Power Spectrum.tdms'):
            continue
        if not file_name.endswith('.tdms'):
            continue
        group_name = data_query.search(file_name)
        if not group_name:
            warning('failed to find group name in file name %s', file_name)
            continue
        print('date: %s - start_idx: %s - end_idx: %s' % (
            group_name.group('date'), group_name.group('start_idx'), group_name.group('end_idx')))
        entry_name = '_'.join((group_name.group('date'), group_name.group('start_idx'), group_name.group('end_idx')))
        data = load_tdms_data(os.path.join(PATH, file_name))
        if data.empty:
            warning('Failed loading data from %s', file_name)
            continue
        data.F.plot().get_figure().savefig(os.path.join(resutls_path, '%s-force.png' % file_name))
        pp.close('all')
        spike_start_times, spike_end_times = get_spikes_start_end(data)
        step_start_end_points = (data.F > STEP_THRESHOLD).astype(int).diff()
        first_step_start = step_start_end_points.nlargest(1).index[0]
        last_step_end = max(
            (step_start_end_points < 0).nlargest(1, keep='last').index[0],
            spike_end_times[-1] if not spike_end_times.empty else first_step_start)
        time_delta = last_step_end - first_step_start
        num_steps = int(round(time_delta.seconds / TIME_OF_CYCLE))
        print('total run time: %s, time between start and end of steps: %s' % (data.index[-1] - data.index[0], time_delta))
        print('num steps: %f' % (time_delta.seconds / TIME_OF_CYCLE))
        print('%s: num spikes found: %d' % (file_name, spike_start_times.shape[0]))
        step_start_times = step_start_end_points.index[step_start_end_points > 0]
        step_end_times = step_start_end_points.index[step_start_end_points < 0]
        spike_distances = []
        spike_forces = []
        for spike_idx, spike_start_time in enumerate(spike_start_times):
            step_end = data.index[data.index.get_loc(spike_start_time - timedelta(seconds=1), method='nearest')]
            step_start = data.index[data.index.get_loc(spike_start_time - timedelta(seconds=2), method='nearest')]
            step_height = data.loc[step_start:step_end].F.mean()
            print('step height for spike at %s is %f' % (spike_start_time, step_height))
            spike_data = data.loc[spike_start_times[spike_idx]:spike_end_times[spike_idx]]
            spike_max_idx = np.argmin(spike_data.F.values)
            spike_height = -spike_data.F.iloc[spike_max_idx]
            print('spike height: %f' % spike_height)
            if spike_max_idx > 1:
                spike_data_sample = spike_data.iloc[:spike_max_idx]
                spike_data_sample = spike_data_sample[spike_data_sample.x2 != 0]
                if spike_data_sample.shape[0] > 1:
                    slope, intercept, r_value, p_value, std_err = linregress(
                        spike_data_sample.x2 - spike_data_sample.x1, spike_data_sample.F)
                    append_spike_reslts(
                        spike_results, '%s: spike %d' % (entry_name, spike_idx), slope, intercept, r_value, p_value,
                        std_err)
            append_results(results, entry_name, spike_height, spike_start_time, step_height)

        for i in range(num_steps - spike_start_times.shape[0]):
            append_results(results, entry_name, 0, pd.NaT, 0)
    except Exception as err:
        warning(err)
        if entry_name:
            append_results(results, entry_name, 0, str(err), 0)
pd.DataFrame(results).to_csv(os.path.join(resutls_path, 'summary.csv'))
pd.DataFrame(spike_results).to_csv(os.path.join(resutls_path, 'slopes.csv'))
print('Done!')